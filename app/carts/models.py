from app.db import db, BaseModelMixin


class Cart(db.Model, BaseModelMixin):
    id = db.Column(db.Integer, primary_key=True)
    country_name = db.Column(db.String)
    vat_id = db.Column(db.String)
    items = db.relationship('Item', backref='cart', lazy=False, cascade='all, delete-orphan')

    def __init__(self, country_name, vat_id, items=[]):
        self.country_name = country_name
        self.vat_id = vat_id
        self.items = items

    def __repr__(self):
        return f'Cart({self.country_name})'

    def __str__(self):
        return f'{self.country_name}'


class Item(db.Model, BaseModelMixin):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    price = db.Column(db.String)

    cart_id = db.Column(db.Integer, db.ForeignKey('cart.id'), nullable=False)

    def __init__(self, name, price):
        self.name = name
        self.price = price

    def __repr__(self):
        return f'Item({self.name})'

    def __str__(self):
        return f'{self.name}'