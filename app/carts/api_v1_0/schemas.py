from marshmallow import fields

from app.ext import ma


class CartSchema(ma.Schema):
    id = fields.Integer(dump_only=True)
    country_name = fields.String()
    vat_id = fields.String()
    items = fields.Nested('ItemSchema', many=True)


class ItemSchema(ma.Schema):
    id = fields.Integer(dump_only=True)
    name = fields.String()
    price = fields.String()