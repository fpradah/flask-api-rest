from flask import request, Blueprint
from flask_restful import Api, Resource

from .schemas import CartSchema
from ..models import Cart, Item

carts_v1_0_bp = Blueprint('carts_v1_0_bp', __name__)

cart_schema = CartSchema()

api = Api(carts_v1_0_bp)

class CartListResource(Resource):
    def get(self):
        carts = Cart.get_all()
        result = cart_schema.dump(carts, many=True)
        return result
    def post(self):
        data = request.get_json()
        cart_dict = cart_schema.load(data)
        cart = Cart(country_name=cart_dict['country_name'],
                    vat_id=cart_dict['vat_id']
        )
        for item in cart_dict['items']:
            cart.items.append(Item(item['name'],item['price']))
        cart.save()
        resp = cart_schema.dump(cart)
        return resp, 201

class CartResource(Resource):
    def get(self, cart_id):
        cart = Cart.get_by_id(cart_id)
        if cart is None:
            raise ObjectNotFound('The cart not exists')
        resp = cart_schema.dump(cart)
        return resp

api.add_resource(CartListResource, '/api/v1.0/carts/', endpoint='cart_list_resource')
api.add_resource(CartResource, '/api/v1.0/carts/<int:cart_id>', endpoint='cart_resource')